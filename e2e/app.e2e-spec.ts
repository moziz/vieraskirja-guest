import { VieraskirjaGuestPage } from './app.po';

describe('vieraskirja-guest App', () => {
  let page: VieraskirjaGuestPage;

  beforeEach(() => {
    page = new VieraskirjaGuestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
