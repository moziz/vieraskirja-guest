# VieraskirjaGuest

Wedding guestbook web client that allows guests to leave messages and draw pictures. All of these are then saved to the server.

In big screen mode, it shows a carousel of existing greetings and receives notificatoins about new greetings from the server through a WebSocket connection.

Server code: https://bitbucket.org/moziz/vieraskirja-server/
