export class Card {
  fileIdentifier: string = null;
  userName: string = null;
  drawingName: string = null;
  bodyText: string = null;
}
