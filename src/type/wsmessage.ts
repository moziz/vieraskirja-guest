export class WSMessage {
  constructor(
    public error: Error,
    public topic: string,
    public data: Object
  ) {
    if (topic === null || topic.length === 0) {
      throw new TypeError("Topic is required");
    }
  }

  get json() {
    return JSON.stringify(this);
  }

  static parse(source: string): WSMessage {
    let plainObject: Object = null;
    try {
      plainObject = JSON.parse(source);
    } catch(e) {
      return null;
    }

    let message: WSMessage = null;

    try {
      message = <WSMessage>plainObject;
    } catch (e) {
      return null;
    }

    if (message.topic === null) {
      return null;
    }

    return message;
  }
}
