import {
  Component, ViewChild, OnInit, OnDestroy, ElementRef,
  Input
} from '@angular/core';

@Component({
  selector: 'app-drawing-display',
  template: `
    {{drawingId}}
    <img [src]="imgSrc" />
  `
})
export class DrawingDisplayComponent implements OnInit, OnDestroy {
  @Input() drawingId: string;

  protected imgSrc: string;

  constructor() {}

  ngOnInit(): void {
    this.imgSrc = `/data/drawings/${this.drawingId}`;
  }

  ngOnDestroy(): void {
  }
}
