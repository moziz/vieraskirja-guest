import { Component, ViewChild, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {WebSocketSubject} from 'rxjs/observable/dom/WebSocketSubject';
import {WSMessage} from '../../type/wsmessage';
import { Card } from '../../type/card';

@Component({
  selector: 'app-collection',
  template: `
    <div class="cards-container">
      <app-card *ngFor="let card of cards" [cardData]="card"></app-card>
    </div>
  `
})
export class CollectionComponent implements OnInit, OnDestroy {
  protected socket: WebSocketSubject<WSMessage> = null;

  protected cards: Array<Card> = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.socket = new WebSocketSubject<WSMessage>({
      url: `ws://${window.location.hostname}:${window.location.port}/`
    });

    this.socket.subscribe(
      (message: WSMessage) => {
        console.log("Got WS message:", message);
        this.onWSMessage(message);
      },
      error => {
        console.error("WebSocket error occured:", error);
        this.socket.complete();
      }
    );

    this.fetchAllCards();
  }

  ngOnDestroy(): void {
  }

  fetchAllCards(): void {
    this.http.get('/api/display/card/all')
      .subscribe(
        (res: Array<Card>) => {
          this.cards.length = 0;

          for (let i = 0; i < res.length; ++i) {
            this.cards.push(res[i]);
          }
        },
        err => {
          console.error("Getting the cards listing failed:", err);
        }
      )
    ;
  }

  fetchCard(id: string): void {
    this.http.get(`/api/display/card/${id}`)
      .subscribe(
        (res: Card) => {
          console.log(res);
          this.cards.push(res);
          window.scroll(-1000000000, 0);
        },
        (err) => {
          console.error("Getting a single card failed:", err);
        }
      );
  }

  onWSMessage(message: WSMessage): void {
    switch (message.topic) {
      case 'new-card':
        this.fetchCard(message.data['id']);
        break;
    }
  }
}
