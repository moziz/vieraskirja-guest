import { Component, ViewChild, OnInit, OnDestroy, ElementRef } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {DrawComponent} from "./draw.component";

@Component({
  selector: 'app-make-card',
  template: `
    <div class="section-nav-container">
      <button *ngIf="sectionIndex > 0" class="section-nav-previous" (click)="onSectionNav(-1)">&lt;</button>
      <div></div>
      <button *ngIf="sectionIndex < 3" class="section-nav-next" (click)="onSectionNav(1)">&gt;</button>
    </div>

    <div [ngClass]="{ 'hidden': sectionIndex !== 0 }" class="section section-self">
      <div class="input-field-container">
        <div>Nimeni</div>
        <input #nameInput type="text" maxlength="20" />
      </div>
      <div class="drawing-title">Näen itseni tällaisena:</div>
      <app-draw #drawingSelf></app-draw>
    </div>

    <div [ngClass]="{ 'hidden': sectionIndex !== 1 }" class="section section-prompt">
      <div class="input-field-container">
        <div>Teokseni nimi</div>
        <input #promptInput type="text" maxlength="80" />
        <button (click)="nextPrompt()">Arvo uusi aihe</button>
      </div>
      <app-draw #drawingPrompt></app-draw>
    </div>

    <div [ngClass]="{ 'hidden': sectionIndex !== 2 }" class="section section-greeting">
      <div class="input-field-container">
        <div>Terveiset</div>
        <input #greetingInput maxlength="256" />
      </div>
    </div>

    <div [ngClass]="{ 'hidden': sectionIndex !== 3 }" class="section section-publish">
      <button (click)="onPublish()">Julkaise kortti</button>
    </div>
  `
})
export class MakeCardComponent implements OnInit, OnDestroy {
  @ViewChild('drawingSelf') drawingSelf: DrawComponent;
  @ViewChild('drawingPrompt') drawingPrompt: DrawComponent;
  @ViewChild('nameInput') nameInput: ElementRef;
  @ViewChild('promptInput') promptInput: ElementRef;
  @ViewChild('greetingInput') greetingInput: ElementRef;

  protected prompts: Array<string> = ["Piirrä jotain kivaa"];
  protected promptIndex: number = 0;
  protected sectionIndex: number = 0;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.fetchPrompts();
  }

  ngOnDestroy(): void {
  }

  fetchPrompts(): void {
    this.http.get('/data/prompts.json').subscribe(
      (res: Array<string>) => {
        this.prompts.length = 0;

        for (let i = 0; i < res.length; ++i) {
          this.prompts.push(res[i]);
        }

        this.nextPrompt();
      },
      err => {
        console.error("Fetching prompts failde:", err);
      }
    );
  }

  nextPrompt(): void {
    this.promptIndex = Math.floor(this.prompts.length * Math.random());
    this.promptInput.nativeElement.value = this.prompts[this.promptIndex];
    console.log(this.promptInput);
  }

  onSectionNav(indexDelta: number): void {
    this.sectionIndex = Math.abs((this.sectionIndex + indexDelta) % 4);
  }

  onPublish(): void {
    if (!confirm("Haluatko varmasti jukaista korttisi?")) {
      return;
    }

    let drawingSelfBlob: Blob = null;
    let drawingPromptBlob: Blob = null;

    const sendForm = (): void => {
      const formData: FormData = new FormData();
      formData.append('drawing', drawingPromptBlob);
      formData.append('avatar', drawingSelfBlob);
      formData.append('userName', this.nameInput.nativeElement.value);
      formData.append('drawingName', this.promptInput.nativeElement.value);
      formData.append('bodyText', this.greetingInput.nativeElement.value);

      this.http.post(
        '/api/submit/card',
        formData,
      ).subscribe(
        (data: any) => {
          alert("Kuva on nyt julkaistu!");
          this.sectionIndex = 0;
        },
        (error: HttpErrorResponse) => {
          alert("Julkaisu epäonnistui! Kuvan lähettäminen epäonnistui.");
        }
      );
    };

    // Lazy async waiting...

    let waitCount: number = 0;

    ++waitCount;
    this.drawingSelf.getImage((result) => {
      --waitCount;

      drawingSelfBlob = result;

      if (waitCount === 0) {
        sendForm();
      }
    });

    ++waitCount;
    this.drawingPrompt.getImage((result) => {
      --waitCount;

      drawingPromptBlob = result;

      if (waitCount === 0) {
        sendForm();
      }
    });
  }
}

class CanvasPoint {
  // Element coords
  ex: number = 0;
  ey: number = 0;

  // Normalized coords
  nx: number = 0;
  ny: number = 0;

  // Canvas pixel coords
  cx: number = 0;
  cy: number = 0;

  static make(canvas: HTMLCanvasElement, windowX: number, windowY: number): CanvasPoint {
    const p = new CanvasPoint();

    p.ex = windowX - canvas.offsetLeft;
    p.ey = windowY - canvas.offsetTop;

    p.nx = p.ex / canvas.offsetWidth;
    p.ny = p.ey / canvas.offsetHeight;

    p.cx = p.nx * canvas.width;
    p.cy = p.ny * canvas.height;

    return p;
  }
}
