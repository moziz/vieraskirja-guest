import { Component, ViewChild, OnInit, OnDestroy, ElementRef, Input } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Card} from "../../type/card";

@Component({
  selector: 'app-card',
  template: `
    <div class="content-container">
      <div class="personal-container">
        <img [src]="'/data/drawings/' + cardData?.fileIdentifier + '.avatar.png'" />
        <div class="user-name">{{cardData?.userName}}</div>
        <div class="greeting">{{cardData?.bodyText}}</div>
      </div>

      <div class="drawing-container">
        <img [src]="'/data/drawings/' + cardData?.fileIdentifier + '.drawing.png'" />
        <div class="drawing-name">{{cardData?.drawingName}}</div>
      </div>
    </div>
  `
})
export class CardComponent implements OnInit, OnDestroy {
  @Input() cardData: Card = null;

  constructor() {}

  ngOnInit(): void {
    console.log(this.cardData);
  }

  ngOnDestroy(): void {
  }
}
