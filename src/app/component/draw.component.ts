import { Component, ViewChild, OnInit, OnDestroy, ElementRef } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-draw',
  template: `
    <div class="canvas-container">
      <canvas #mainCanvas width="256" height="256"
        (mousedown)="onDrawBegin($event, 1)"
        (touchstart)="onDrawBegin($event, 1)"
        (window:mouseup)="onDrawEnd($event, 1)"
        (window:touchend)="onDrawEnd($event, 1)"
        (mousemove)="onDrawMove($event, 1)"
        (touchmove)="onDrawMove($event, 1)"
      ></canvas>
    </div>

    <div class="toolbar">
      <div class="brushes">
        <button (click)="setBrushSize(0)" [ngClass]="{ 'selected': brushSizeIndex === 0 }">S</button>
        <button (click)="setBrushSize(1)" [ngClass]="{ 'selected': brushSizeIndex === 0 }">L</button>
      </div>
      <div class="colors">
        <button
          *ngFor="let colors of colorPallettes[palletIndex]; let i = index"
          (click)="onChooseColor(i)"
          [ngStyle]="{ 'background-color': colorPallettes[palletIndex][i] }"
        ></button>
      </div>
      <div class="actions">
        <button (click)="onClear()">X</button>
      </div>
    </div>
  `
})
export class DrawComponent implements OnInit, OnDestroy {
  @ViewChild('mainCanvas') mainCanvas: ElementRef;

  protected brushSizes: Array<number> = [8, 16];
  protected brushSizeIndex: number = 0;
  protected ctx: CanvasRenderingContext2D = null;
  protected lastPos: CanvasPoint = new CanvasPoint();
  protected isDrawing: boolean = false;

  protected colorIndex: number = 1;
  protected palletIndex: number = 0;
  protected colorPallettes: Array<Array<string>> = [
    ['#c5d86d', '#2e294e', '#1b998b', '#d7263d'],
    ['#fae3c6', '#f564a9', '#faa4bd', '#533b4d'],
    ['#1ea896', '#ff715b', '#ffffff', '#4c5454'],
    ['#cfffb0', '#e03616', '#fff689', '#546a76'],
    ['#dad3c9', '#8ba0a7', '#b7cdb4', '#546a76'],
    ['#d7c0d0', '#eff0d1', '#77ba99', '#262730'],
    ['#f4d58d', '#708d81', '#bf0603', '#000000']
  ];

  constructor() {}

  ngOnInit(): void {
    this.ctx = this.mainCanvas.nativeElement.getContext('2d');

    this.onPickNewPallet();
    this.clearCanvas();
  }

  ngOnDestroy(): void {
  }

  clearCanvas(fillColorIndex: number = 0): void {
    this.ctx.fillStyle = this.colorPallettes[this.palletIndex][fillColorIndex];
    this.ctx.fillRect(0, 0, this.mainCanvas.nativeElement.width, this.mainCanvas.nativeElement.height);
  }

  setBrushSize(index: number): void {
    this.brushSizeIndex = Math.abs(index % this.brushSizes.length);
  }

  onPickNewPallet(): void {
    this.palletIndex = Math.floor(Math.random() * this.colorPallettes.length);
    this.colorIndex = Math.floor(Math.random() * (this.colorPallettes[this.palletIndex].length - 1) + 1);
  }

  onDrawBegin(event: any): void {
    this.lastPos = this.makeCanvasPoint(event);
    this.isDrawing = true;

    event.preventDefault();
  }

  onDrawEnd(event: any): void {
    this.isDrawing = false;
  }

  onDrawMove(event: any): void {
    const pos = this.makeCanvasPoint(event);

    if (this.isDrawing) {
      this.ctx.strokeStyle = this.colorPallettes[this.palletIndex][this.colorIndex];
      this.ctx.lineCap = 'round';
      this.ctx.lineWidth = this.brushSizes[this.brushSizeIndex];

      this.ctx.beginPath();
      this.ctx.moveTo(this.lastPos.cx, this.lastPos.cy);
      this.ctx.lineTo(pos.cx, pos.cy);
      this.ctx.stroke();

      this.lastPos = pos;
    }

    event.preventDefault();
  }

  onChooseColor(index: number): void {
    this.colorIndex = index;
  }

  getImage(cb): void {
    this.mainCanvas.nativeElement.toBlob(cb, 'image/png');
  }

  onClear(): void {
    const clear: boolean = confirm("Haluatko varmasti pyyhkiä piirustuksesi?");

    if (clear) {
      this.clearCanvas();
    }
  }

  makeCanvasPoint(event: any): CanvasPoint {
    return CanvasPoint.make(
      this.mainCanvas.nativeElement,
      event.pageX || (event.changedTouches && event.changedTouches[0] && event.changedTouches[0].pageX) || 0,
      event.pageY || (event.changedTouches && event.changedTouches[0] && event.changedTouches[0].pageY) || 0
    );
  }
}

class CanvasPoint {
  // Element coords
  ex: number = 0;
  ey: number = 0;

  // Normalized coords
  nx: number = 0;
  ny: number = 0;

  // Canvas pixel coords
  cx: number = 0;
  cy: number = 0;

  static make(canvas: HTMLCanvasElement, windowX: number, windowY: number): CanvasPoint {
    const p = new CanvasPoint();

    p.ex = windowX - canvas.offsetLeft;
    p.ey = windowY - canvas.offsetTop;

    p.nx = p.ex / canvas.offsetWidth;
    p.ny = p.ey / canvas.offsetHeight;

    p.cx = p.nx * canvas.width;
    p.cy = p.ny * canvas.height;

    return p;
  }
}
