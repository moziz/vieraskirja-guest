import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DrawComponent } from './component/draw.component';
import { CollectionComponent } from './component/collection.component';
import {DrawingDisplayComponent} from "./component/drawing-display.component";
import {MakeCardComponent} from "./component/make-card.component";
import {CardComponent} from "./component/card.component";

@NgModule({
  declarations: [
    AppComponent,
    DrawComponent,
    CollectionComponent,
    DrawingDisplayComponent,
    MakeCardComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
