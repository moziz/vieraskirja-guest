import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-make-card *ngIf="views[currentViewIndex] == 'drawing'"></app-make-card>
    <app-collection *ngIf="views[currentViewIndex] == 'collection'"></app-collection>
  `
})
export class AppComponent implements OnInit, OnDestroy {
  protected views: Array<string> = ['drawing', 'collection'];
  protected currentViewIndex: number = 0;

  constructor() {}

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  @HostListener('window:keydown', ['$event'])
  onKeyboardInput(event: KeyboardEvent) {
    if (event.key === 'F1') {
      this.currentViewIndex = (this.currentViewIndex + 1) % this.views.length;
      event.preventDefault();
    }
  }
}
